Openssl TCP 예제 실행 방법

1. openssl 및 라이브러리 설치
sudo apt-get install libssl-dev openssl

2. 인증서 생성
$ openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout key.pem -out cert.pem

3. Build
$ make

4. Test 
- 서버 실행
$ ./openssl_server 
- 클라이언트 실행
  args[1]에 보낼 메시지(default: "Hello")
$ ./openssl_client MESSAGE 
